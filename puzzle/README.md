Puzzle project by Yanir & Israel
====================

We were very careful to comply with all of the requirements, hopefully we did not miss anything.
Anyway we'll be glad to your comments :)

On this page:

* [Requirements](#markdown-header-Requirements)

* [Server](#markdown-header-Server)
	
* [Client](#markdown-header-Client)

* [Testing](#markdown-header-Testing)

* [UML diagrams](#markdown-header-UML diagrams)

- - -

# Requirements

Below you can see the detailed requirements in PDF format:

[Part1 - the basic requirements] [part1]

[Part2 - client-server enhancement] [part2]


# Server

To run the server process - execute the `Main` method in `GameServer` java class with the parameters described below.

When raising the server there would be two command line parameters:

*	Optional - indicating number of threads to use (including main). If not provided default will be 4.

		-threads <num_threads>

*	Optional - indicating port number to listen on. If not provided default will be 7095.

		-port <port_number>
	
Server will listen to clients trying to connect to the specified (or default) port.


# Client

To run the client process - execute the `Main` method in `GameClient` java class with the parameters described below.

Client has the following command line parameters:

*	Optional - the server IP address, if not provided default will be 127.0.0.1
		
		-ip <ip_address>


* 	Optional - the server port, if not provided should be as the default of the server

		-port <port_number>

*	**Optional (not as required)** - position of the puzzle file to solve - not in JSON format.
 		
		-input <input_file_name>

	We have decided that it should be optional.
	If the user is not providing this argument - a file selection popup will come up and will allow the user to select the input file more easily.

* **Optional (not as required)** - position of the puzzle output file - NOT in JSON format.

		-output <output_file_name>

	We have decided to provide a default output name in case no such argument was provided by the user.
	The default name is the input file name + "Results".
	It can be easly switched to being mandatory by the relevant annotation.
	
# Testing

The project has above 90% of Unit tests coverage.

We used [JUnit5] and [JMockit] frameworks for the tests.


# UML diagrams

The diagrams below were **generated automatically** in order to show the general structure and flow of the system.

We are aware that it's not perfect, but we decided to show it anyway since it helps to understand the system operation.


### Class diagram for server-side:

![Alt text][ServerClassDiagram]

### Sequence diagram for server-side:

![Alt text][ServerSequenceDiagram]

### Class diagram for client-side:

![Alt text][ClientClassDiagram]

### Sequence diagram for client-side:

![Alt text][ClientSequenceDiagram]

- - -

- - -


[part1]: http://bystro-kuplu.ru/images/BOOST_Puzzle_Project_part1.pdf
[part2]: http://bystro-kuplu.ru/images/BOOST_Puzzle_Project_part3_networking.pdf
[ServerClassDiagram]: http://bystro-kuplu.ru/images/ServerClassDiagram.png
[ServerSequenceDiagram]: http://bystro-kuplu.ru/images/ServerSequenceDiagram.png
[ClientClassDiagram]: http://bystro-kuplu.ru/images/ClientClassDiagram.png
[ClientSequenceDiagram]: http://bystro-kuplu.ru/images/ClientSequenceDiagram.png
[JMockit]: https://jmockit.org/
[JUnit5]: https://junit.org/junit5/