package boost.puzzle.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.math.NumberUtils;
import org.codehaus.plexus.util.StringUtils;

import boost.puzzle.ErrorsManager;
import boost.puzzle.consts.ConfigurationConsts;
import boost.puzzle.consts.Error;
import boost.puzzle.models.Piece;

public class Parser {

	private int numberOfElemntsDeclared;
	private List<String> linesFromFile;
	private List<Piece> puzzlePieces = new ArrayList<>();
	private ErrorsManager errorsManager;

	public Parser(List<String> linesFromFile, ErrorsManager errorsManager) {
		this.linesFromFile = linesFromFile;
		this.errorsManager = errorsManager;
	}

	public List<Piece> parse() {
		removeIgnorableLines();

		validateFile();

		return puzzlePieces;
	}

	private void validateFile() {
		verifyFirstLine();

		verifyAllOtherLines();

		verifyElementsIds();
	}

	private void verifyElementsIds() {
		Map<Integer, List<Piece>> piecesById = puzzlePieces.stream().collect(Collectors.groupingBy(Piece::getId));

		checkForMissingIds(piecesById.keySet());

		checkForWrongIds();

		checkForDuplicateIds(piecesById);
	}

	private void checkForWrongIds() {
		if (!errorsManager.getWrongIds().isEmpty()) {
			errorsManager.addError(Error.ID_EXEEDS_SIZE, numberOfElemntsDeclared, errorsManager.getWrongIds());
		}
	}

	private void removeIgnorableLines() {
		// Remove comment lines (#) and blank lines
		linesFromFile = linesFromFile.stream().filter(l -> !l.startsWith(ConfigurationConsts.COMMENT_IDENTIFIER))
		        .filter(l -> StringUtils.isNotBlank(l)).collect(Collectors.toList());
	}

	private void checkForDuplicateIds(Map<Integer, List<Piece>> piecesById) {
		for (Entry<Integer, List<Piece>> entry : piecesById.entrySet()) {
			if (entry.getValue().size() > 1) {
				errorsManager.addDuplicateId(entry.getKey());
			}
		}

		if (!errorsManager.getDuplicateIds().isEmpty()) {
			errorsManager.addError(Error.DUPLICATE_IDS, errorsManager.getDuplicateIds());
		}

	}

	private void checkForMissingIds(Set<Integer> piecesIds) {
		for (int i = 1; i <= numberOfElemntsDeclared; i++) {
			if (!piecesIds.contains(i)) {
				errorsManager.addMissingId(i);
			}
		}

		if (!errorsManager.getMissingIds().isEmpty()) {
			errorsManager.addError(Error.MISSING_ELEMENT, errorsManager.getMissingIds());
		}
	}

	private void verifyAllOtherLines() {
		for (int i = 1; i < linesFromFile.size(); i++) {
			verifyPieceLine(linesFromFile.get(i), i);
		}
	}

	private void verifyPieceLine(String line, int lineIndex) {
		boolean isAllNumeric = false;
		boolean isFiveValues = false;
		boolean isIdValid = false;
		boolean isEdgesValid = false;

		int[] valuesAsInts = null;
		
		line = line.trim();

		String[] separatedLine = line.split(ConfigurationConsts.REGEX_ANY_WHITE_SPACES);
		
		isAllNumeric = isAllNumeric(separatedLine, lineIndex);
		valuesAsInts = parseAsInt(isAllNumeric, separatedLine);

		int id = 0;
		if (isAllNumeric) {
			id = valuesAsInts[0];
			isIdValid = isIdValid(id);
		}

		isFiveValues = isFiveValues(lineIndex, separatedLine, id);

		if (isAllNumeric && isIdValid && isFiveValues) {
			isEdgesValid = isEdgesValid(line, valuesAsInts);
		}

		if (isEdgesValid) {
			addPiece(valuesAsInts, id);
		}
	}

	private boolean isEdgesValid(String line, int[] valuesAsInts) {
		boolean result = true;

		for (int i = 1; i < valuesAsInts.length; i++) {
			if (valuesAsInts[i] != ConfigurationConsts.EDGE_STRAIGHT && valuesAsInts[i] != ConfigurationConsts.EDGE_INTERIOR
			        && valuesAsInts[i] != ConfigurationConsts.EDGE_EXTERIOR) {
				errorsManager.addError(Error.WRONG_ELEMENT_FORMAT, valuesAsInts[0], line);
				result = false;
			}
		}

		return result;
	}

	private void addPiece(int[] valuesAsInts, int id) {
		Piece piece = new Piece(id, valuesAsInts[1], valuesAsInts[2], valuesAsInts[3], valuesAsInts[4]);
		puzzlePieces.add(piece);
	}

	private boolean isIdValid(int id) {
		if (id < ConfigurationConsts.MINIMUM_PIECE_ID || id > numberOfElemntsDeclared) {
			errorsManager.addWrongId(id);
			return false;
		} else {
			return true;
		}
	}

	private boolean isFiveValues(int lineIndex, String[] separatedLine, int id) {
		boolean result = false;

		if (separatedLine.length != ConfigurationConsts.NUMBER_OF_VALUES_PER_PIECE) {
			errorsManager.addError(Error.WRONG_ELEMENT_FORMAT, id, linesFromFile.get(lineIndex));
		} else {
			result = true;
		}

		return result;
	}

	private int[] parseAsInt(boolean allNumeric, String[] separatedLine) {
		int[] valuesAsInts = null;

		if (allNumeric) {
			valuesAsInts = new int[5];

			for (int i = 0; i < ConfigurationConsts.NUMBER_OF_VALUES_PER_PIECE; i++) {
				valuesAsInts[i] = Integer.parseInt(separatedLine[i]);
			}
		}
		return valuesAsInts;
	}

	private boolean isAllNumeric(String[] separatedLine, int lineIndex) {
		boolean result = true;

		for (String value : separatedLine) {
			if (!NumberUtils.isNumber(value)) {
				errorsManager.addError(Error.PIECE_VALUE_NOT_NUMERIC, value, lineIndex);
				result = false;
			}
		}

		return result;
	}

	private void verifyFirstLine() {
		String firstLine = linesFromFile.get(0);
		String[] splitFirstLine = firstLine.split(ConfigurationConsts.EQUALS_SIGN);

		String beforeEqualsSign = splitFirstLine[0].trim();
		String afterEqualsSign = splitFirstLine[1].trim();

		if (!beforeEqualsSign.equals(ConfigurationConsts.NUM_ELEMENTS)) {
			errorsManager.addError(Error.FIRST_LINE_BEFORE_SIGN, beforeEqualsSign);
		}

		if (!StringUtils.isNumeric(afterEqualsSign)) {
			errorsManager.addError(Error.FIRST_LINE_AFTER_SIGN, afterEqualsSign);
		} else {
			numberOfElemntsDeclared = Integer.parseInt(afterEqualsSign);
		}

	}

}
