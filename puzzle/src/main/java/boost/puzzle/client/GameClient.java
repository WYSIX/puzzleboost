package boost.puzzle.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import boost.puzzle.ErrorsManager;
import boost.puzzle.consts.ConfigurationConsts;
import boost.puzzle.consts.Error;
import boost.puzzle.models.Piece;
import boost.puzzle.models.Puzzle;
import boost.puzzle.models.Response;
import boost.puzzle.models.ResponseWrapper;
import boost.puzzle.models.SolutionWrapper;
import boost.puzzle.utils.FileUtils;
import boost.puzzle.utils.JsonUtils;

public class GameClient {

	private List<Piece> puzzlePieces;
	private ErrorsManager errorsManager = new ErrorsManager();

	public static void main(String[] args) throws IOException {
		ClientConsoleArgumentsHandler clientConsoleArgumentsHandler = new ClientConsoleArgumentsHandler();
		clientConsoleArgumentsHandler.parse(args);

		GameClient client = new GameClient();
		client.start(clientConsoleArgumentsHandler.getIpAddress(), clientConsoleArgumentsHandler.getPort(),
				clientConsoleArgumentsHandler.getInputFileName(), clientConsoleArgumentsHandler.getOutputFileName());
	}

	public void start(String hostname, int port, String filePath, String outputFileName) {
		try (Socket socket = new Socket(hostname, port)) {
			if (filePath == null) {
				filePath = FileUtils.chooseFile();
			}
			if (outputFileName != null) {
				FileUtils.setOutputFileNameFromUser(outputFileName);
			}
			play(socket, filePath, outputFileName);
		} catch (IOException e) {
			System.out.println("Could not connect to server. Aborting client");
			return;
		}

	}

	public void play(Socket socket, String filePath, String outputFileName) throws IOException {
		Path inputFile = Paths.get(filePath);
		List<String> linesFromFile = null;

		try {
			linesFromFile = FileUtils.readTextFile(inputFile, ConfigurationConsts.FILE_ENCODING);
		} catch (IOException e) {
			printUsageCantReadFile(e);
			return;
		}

		Parser parser = new Parser(linesFromFile, errorsManager);
		puzzlePieces = parser.parse();

		if (!errorsManager.isEmpty()) {
			FileUtils.printToFile(filePath, errorsManager.getErrorsMap());
			return;
		}

		Puzzle puzzle = new Puzzle(puzzlePieces);
		JsonElement jsonRequest = JsonUtils.convertListOfPiecesToJsonElement(puzzle);

		// Send jsonRequest to Puzzle Server by socket
		String responseJson = null;
		try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
				DataInputStream dataInputStream = new DataInputStream(socket.getInputStream())) {
			// Send pieces as a JSON string to server
			System.out.println(jsonRequest.toString());
			dataOutputStream.writeUTF(jsonRequest.toString());

			// Receive PuzzleRecieved message
			responseJson = dataInputStream.readUTF();
			System.out.println(responseJson);

			// Receive response as a JSON string from server
			responseJson = dataInputStream.readUTF();
			System.out.println(responseJson);
		}

		ResponseWrapper responseWrapper = new Gson().fromJson(responseJson, ResponseWrapper.class);
		Response response = responseWrapper.getResponse();

		if (response.hasErrors()) {
			FileUtils.printToFile(filePath, response.getErrors());
		} else if (!response.doesSolutionExist()) {
			errorsManager.addError(Error.NO_SOLUTION);
			response.addErrorsManager(errorsManager);
			FileUtils.printToFile(filePath, response.getErrors());
		} else {
			List<String> generateSolutionPrintout = generateSolutionPrintout(response.getSolutionWrapper());
			FileUtils.printToFile(filePath, generateSolutionPrintout);
		}
	}

	public static List<String> generateSolutionPrintout(SolutionWrapper solutionWrapper) {
		List<String> solutionPrintOut = new ArrayList<>();

		int elementsPerRow = solutionWrapper.getSolution().size() / solutionWrapper.getRows();
		int rowCounter = 0;

		List<String> currentRow = new ArrayList<>();
		for (int elementId = 0; elementId < solutionWrapper.getSolution().size(); elementId++) {

			currentRow.add(String.valueOf(solutionWrapper.getSolution().get(elementId)));
			rowCounter++;

			if (rowCounter == elementsPerRow) {
				solutionPrintOut.add(String.join(" ", currentRow));
				currentRow.clear();
				rowCounter = 0;
			}
		}
		return solutionPrintOut;
	}

	private void printUsageCantReadFile(IOException e) {
		System.out.println(String.format(Error.COULD_NOT_READ_FILE.getMessage(), e.getMessage()));
	}

}
