package boost.puzzle.client;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import boost.puzzle.consts.ConfigurationConsts;

public class ClientConsoleArgumentsHandler {

	@Option(name = "-ip", usage = "ip <ip_address>")
	private String ipAddress = ConfigurationConsts.DEFAULT_SERVER_ADDRESS;

	@Option(name = "-port", usage = "-port <port_number>")
	private int port = ConfigurationConsts.DEFAULT_PORT_NUMBER;

	@Option(name = "-input", usage = "-input <input_file_name>")
	private String inputFileName;

	@Option(name = "-output", usage = "-output <output_file_name>")
	private String outputFileName;

	public String getIpAddress() {
		return ipAddress;
	}

	public int getPort() {
		return port;
	}

	public String getInputFileName() {
		return inputFileName;
	}

	public String getOutputFileName() {
		return outputFileName;
	}

	public void parse(String[] args) {
		CmdLineParser parser = new CmdLineParser(this);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			parser.printUsage(System.err);
			System.err.println();
			return;
		}
	}
}
