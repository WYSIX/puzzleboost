package boost.puzzle.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;

import boost.puzzle.consts.ConfigurationConsts;

public class FileUtils {

	private static final String FILE_SUFFIX = ".txt";
	private static final String ADDED_STRING_FOR_OUTPUT_FILE = "Results";

	public static String outputFileNameFromUser;

	public static String getOutputFileNameFromUser() {
		return outputFileNameFromUser;
	}

	public static void setOutputFileNameFromUser(String inputFileNameFromUser) {
		FileUtils.outputFileNameFromUser = inputFileNameFromUser;
	}

	public static List<String> readTextFile(Path filePath, String encoding) throws IOException {
		List<String> linesFromFile = new ArrayList<>();

		try (BufferedReader in = new BufferedReader(
		        new InputStreamReader(new FileInputStream(filePath.toString()), encoding))) {

			String line;
			while ((line = in.readLine()) != null) {
				linesFromFile.add(line);
			}

		}

		return linesFromFile;
	}

	public static void writeTextFile(Path filePath, String encoding, List<String> linesToPrint) throws IOException {
		try (BufferedWriter out = new BufferedWriter(
		        new OutputStreamWriter(new FileOutputStream(filePath.toString()), encoding))) {

			int linesCounter = 0;
			for (String line : linesToPrint) {
				linesCounter++;
				out.write(line);

				if (linesCounter < linesToPrint.size()) {
					out.newLine();
				}
			}

		}

		System.out.println(String.format("File created: <%s>", filePath.getFileName()));
	}

	public static void printToFile(String filePath, List<String> strings) throws IOException {
		Path outputFile = Paths.get(filePath);

		String outputFileName = "";

		if (outputFileNameFromUser != null) {
			outputFileName = outputFileNameFromUser;
		} else {
			outputFileName = FileUtils.generateOutputFileName(outputFile.getFileName().toString());
		}

		String newPath = outputFile.toFile().getParent() + File.separator + outputFileName;
		
		FileUtils.writeTextFile(Paths.get(newPath), ConfigurationConsts.FILE_ENCODING, strings);
	}

	public static String generateOutputFileName(String intputFileName) {
		String result = intputFileName;

		if (intputFileName.contains(FILE_SUFFIX)) {
			result = intputFileName.substring(0, intputFileName.indexOf(FILE_SUFFIX));
		}

		return result + ADDED_STRING_FOR_OUTPUT_FILE + FILE_SUFFIX;
	}

	public static String chooseFile() {
		JFileChooser fc = new JFileChooser();
		fc.setCurrentDirectory(new File(ConfigurationConsts.INPUT_FILES_PATH));
		int returnVal = fc.showOpenDialog(fc);

		File file = null;
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			file = fc.getSelectedFile();
		}

		return file.getPath();
	}

}
