package boost.puzzle.utils;

import boost.puzzle.models.Piece;
import boost.puzzle.server.Solution;
import boost.puzzle.server.Pattern.EDGE;

public class SolutionChecker {
    private Solution checkedSolution;
    private int rowSize;
    private int colSize;
    private int row = 0;
    private int col = -1;

    public SolutionChecker(Solution checkedSolution) {
        this.checkedSolution = checkedSolution;
    }

    public boolean isSolutionGood() {
        //the first number in checkedSolution is the row size of solution
        rowSize = checkedSolution.getRows();
        colSize = checkedSolution.getCols();
        for (int i = 0; i < checkedSolution.getSolutionPieces().size(); i++) {
            if (!isMatch(i)) {
                return false;
            }
        }
        return true;
    }

    private int getRow(int i) {
        if (i != 0 && i % colSize == 0) {
            row++;
        }
        return row;
    }

    private int getCol(int i) {
        if (i != 0 && i % colSize == 0) {
            col = -1;
        }
        col++;
        return col;
    }

    private boolean isMatch(int i) {
        int row = getRow(i);
        int col = getCol(i);
        Piece piece = checkedSolution.getPieceBySolutionIndex(i);

        // for extreme row/col must has 0 from the relevant side
        if (row == 0 && piece.getEdge(EDGE.TOP) != 0) {
            return false;
        } else if (col == 0 && piece.getEdge(EDGE.LEFT) != 0) {
            return false;
        } else if (row == rowSize - 1 && piece.getEdge(EDGE.BOTTOM) != 0) {
            return false;
        } else if (col == colSize - 1 && piece.getEdge(EDGE.RIGHT) != 0) {
            return false;
            // must match with the neighbors from the left and top
        } else if (col > 0 && piece.getEdge(EDGE.LEFT)
                + checkedSolution.getPieceBySolutionIndex(i - 1).getEdge(EDGE.RIGHT) != 0) {
            return false;
        } else if (row > 0 && piece.getEdge(EDGE.TOP)
                + checkedSolution.getPieceBySolutionIndex((i - colSize)).getEdge(EDGE.BOTTOM) != 0) {
            return false;
        }
        return true;
    }
}