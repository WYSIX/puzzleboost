package boost.puzzle.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import boost.puzzle.models.Puzzle;
import boost.puzzle.models.RequestWrapper;

public class JsonUtils {

	private static Gson gson = new Gson();

	public static String createJsonStringFromObject(Object object) {
		return gson.toJson(object);
	}

	public static JsonElement convertListOfPiecesToJsonElement(Puzzle puzzle) {
		RequestWrapper requestWrapper = new RequestWrapper(puzzle);
		JsonElement jsonRequest = gson.toJsonTree(requestWrapper);
		return jsonRequest;
	}

	public static <T> String toJsonStringWrappedWithRoot(Object object, String wrapperTitle) {
		JsonElement jsonElement = gson.toJsonTree(object);
		JsonObject jsonObject = new JsonObject();
		jsonObject.add(wrapperTitle, jsonElement);
		return jsonObject.toString();
	}

}
