package boost.puzzle.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class GameServer {

	public static void main(String[] args) {
		ServerConsoleArgumentsHandler serverConsoleArgumentsHandler = new ServerConsoleArgumentsHandler();
		serverConsoleArgumentsHandler.parse(args);

		GameServer server = new GameServer();
		server.start(serverConsoleArgumentsHandler.getThreads(), serverConsoleArgumentsHandler.getPort());
	}

	public void start(int numberOfThreadsInPool, int port) {
		System.out.println(String.format("Server is running on port <%s>", port));
		boolean keepGoing = true;
		int sessionId = 1;

		ExecutorService executor = Executors.newFixedThreadPool(numberOfThreadsInPool);

		try (ServerSocket server = new ServerSocket(port)) {
			while (keepGoing) {
				Socket socket = server.accept(); // blocking
				ClientHandler handler = new ClientHandler(socket, sessionId++);

				executor.execute(handler);
			}
		} catch (IOException e) {
			// Ignore
			System.out.println("Server has stopped :" + e.getMessage());
		}
	}

}
