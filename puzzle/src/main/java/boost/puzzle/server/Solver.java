package boost.puzzle.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.JsonElement;

import boost.puzzle.ErrorsManager;
import boost.puzzle.models.Piece;
import boost.puzzle.models.Response;
import boost.puzzle.models.ResponseWrapper;
import boost.puzzle.models.SolutionWrapper;

public class Solver {
	private List<Piece> pieces;
	private List<Integer> validatedRowsNumber = new ArrayList<>();
	private ErrorsManager errorsManager;
	private Map<Integer, List<Piece>> piecesLeft = new HashMap<>();
	private Solution successSolution = new Solution(new ArrayList<>());

	public Solver() {
		this.errorsManager = new ErrorsManager();
	}

	public JsonElement start(List<Piece> pieces) {
		setPieces(pieces);
		PiecesValidator piecesValidator = new PiecesValidator(pieces, errorsManager);
		Response response = new Response();
		piecesValidator.checkSumOfEdgesIsZero();

		if (!errorsManager.isEmpty()) {
			response.addErrorsManager(errorsManager);
			return new ResponseWrapper(response).serializeExpose();
		}

		piecesValidator.calcPossibleRowsNumber();
		validatedRowsNumber = piecesValidator.validatePossibleRowsNumber();
		if (!errorsManager.isEmpty()) {
			response.addErrorsManager(errorsManager);
			return new ResponseWrapper(response).serializeExpose();
		}

		else if (solveAnySize()) {
			response.addSolutionWrapper(new SolutionWrapper(successSolution));
			return new ResponseWrapper(response).serializeExpose();
		} else {
			response.setSolutionExistsFalse();
		}

		return new ResponseWrapper(response).serialize();
	}

	public boolean solveAnySize() {

		indexAllPieces();

		for (Integer currentRowNumber : validatedRowsNumber) {

			Solution possibleSolution = new Solution(new ArrayList<>());
			possibleSolution.setSizes(currentRowNumber, pieces.size());

			if (findMatchPieceForLocation(possibleSolution, piecesLeft)) {
				return true;
			}
		}
		return false;
	}

	private boolean findMatchPieceForLocation(Solution possibleSolution,
	        Map<Integer, List<Piece>> piecesLeft) {
		if (possibleSolution.getSolutionPieces().size() == pieces.size()) { // if was solved
			return true;
		}

		Pattern patternForNext = possibleSolution.getPatternForNextPiece();
		List<Piece> matchingPieces = getMatchingPiecesByPattern(patternForNext, piecesLeft);

		if (matchingPieces == null) {
			return false;
		}
		for (Piece matchingPiece : matchingPieces) {

			if (possibleSolution.doesMatchIndexed(matchingPiece)) {
				// Create and update the new list of left pieces and the new solution
				Map<Integer, List<Piece>> newPiecesLeft = deepCopy(piecesLeft);

				Solution newSolution = new Solution(possibleSolution);

				newSolution.add(matchingPiece);
				removePiece(newPiecesLeft, matchingPiece);

				// recursion - try to fill the next position by any of the pieces left
				if (findMatchPieceForLocation(newSolution, newPiecesLeft)) {
					if (successSolution.isEmpty()) {
						successSolution = new Solution(newSolution);
					}
					return true;
				}
			}
		}
		return false;
	}

	public Solution getSolution() {
		return successSolution;
	}

	// this method is for testing purpose only
	/* package */ void setValidatedRowsNumber(ArrayList<Integer> validatedRowNumber) {
		this.validatedRowsNumber = validatedRowNumber;
	}

	// this method is for testing purpose only
	/* package */ void setPieces(List<Piece> pieces) {
		this.pieces = pieces;
	}

	public void indexAllPieces() {

		for (Piece piece : pieces) {
			addToMap(piece);
		}
	}

	public synchronized void addToMap(Piece piece) {
		Integer topLeftPattern = piece.getTopLeftPattern();

		// if piecesListForPattern does not exist - create it
		List<Piece> piecesListForPattern = piecesLeft.getOrDefault(topLeftPattern, new ArrayList<>());
		piecesListForPattern.add(piece);
		piecesLeft.put(topLeftPattern, piecesListForPattern);
	}

	public List<Piece> getMatchingPiecesByPattern(Pattern topLeftPattern, Map<Integer, List<Piece>> piecesLeft) {

		return piecesLeft.getOrDefault(topLeftPattern.hashCode(), null);
	}

	public void removePiece(Map<Integer, List<Piece>> newPiecesLeft, Piece piece) {
		if (newPiecesLeft.containsKey(piece.getTopLeftPattern())) {
			List<Piece> piecesList = newPiecesLeft.get(piece.getTopLeftPattern());
			piecesList.remove(piece);
			if (piecesList.isEmpty()) {
				newPiecesLeft.remove(piece.getTopLeftPattern());
			}
		}
	}

	public static HashMap<Integer, List<Piece>> deepCopy(Map<Integer, List<Piece>> original) {
		HashMap<Integer, List<Piece>> copy = new HashMap<>();
		for (Map.Entry<Integer, List<Piece>> entry : original.entrySet()) {
			copy.put(entry.getKey(), new ArrayList<>(entry.getValue()));
		}
		return copy;
	}
}