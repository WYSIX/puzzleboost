package boost.puzzle.server;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import boost.puzzle.consts.ConfigurationConsts;

public class ServerConsoleArgumentsHandler {

	@Option(name = "-threads", usage = "-threads <num_threads>")
	private int threads = ConfigurationConsts.DEFAULT_THREADS_NUMBER;

	@Option(name = "-port", usage = "-port <port_number>")
	private int port = ConfigurationConsts.DEFAULT_PORT_NUMBER;

	public int getThreads() {
		return threads;
	}

	public int getPort() {
		return port;
	}

	public void parse(String[] args) {
		CmdLineParser parser = new CmdLineParser(this);
		try {
			parser.parseArgument(args);
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			parser.printUsage(System.err);
			System.err.println();
			return;
		}
	}
}
