package boost.puzzle.server;

import boost.puzzle.ErrorsManager;
import boost.puzzle.consts.Error;
import boost.puzzle.models.Piece;
import boost.puzzle.server.Pattern.EDGE;

import java.util.*;
import java.util.stream.Collectors;

import static java.lang.Math.round;
import static java.lang.Math.sqrt;

public class PiecesValidator {

    private static final String TR = "TR";
    private static final String TL = "TL";
    private static final String BL = "BL";
    private static final String BR = "BR";

    private List<Piece> pieces;
    private ErrorsManager errorsManager;
    private List<Integer> possibleRowsNumber = new ArrayList<>();


    public PiecesValidator(List<Piece> pieces, ErrorsManager errorsManager) {
        this.pieces = pieces;
        this.errorsManager = errorsManager;
    }

    /* package */ void calcPossibleRowsNumber() {

        //optimization - at first try the average row sizes (not extreme values)
        int ave = (int) round(sqrt(pieces.size()));

        int i = ave;
        while (i <= pieces.size()) {
            if (pieces.size() % i == 0) {
                possibleRowsNumber.add(i);
            }
            i++;
        }
        i = ave - 1;
        int j = 1;
        while (i > 0) {
            if (pieces.size() % i == 0) {
                possibleRowsNumber.add(j, i);
                j = j + 2;
            }
            i--;
        }
    }

    /*package*/ List <Integer> validatePossibleRowsNumber() {
        PiecesValidator piecesValidator = new PiecesValidator(pieces, errorsManager);
        List<Integer> validatedRowsNumber = new ArrayList<>(possibleRowsNumber);

        //assume these errors exist, otherwise validate method will withdraw them
        errorsManager.addError(Error.MATCHING_STRAIGHT);
        Set<String> missingCorners = new LinkedHashSet<>(Arrays.asList(TL, TR, BL, BR));

        for (Integer rowNumber : possibleRowsNumber) {
            Set<String> currentMissingCorners = piecesValidator.getMissingCorners(rowNumber);
            missingCorners.retainAll(currentMissingCorners);

            boolean isGoodCornersNumber = currentMissingCorners.isEmpty();
            boolean isGoodStraightEdges = piecesValidator.isCorrectNumberOfStraightEdges(rowNumber);

            if (!isGoodCornersNumber || !isGoodStraightEdges) {
                validatedRowsNumber.remove(rowNumber);
            }
        }
        for (String missingCorner : missingCorners) {
            errorsManager.addError(Error.MISSING_CORNER, missingCorner);
        }

        return validatedRowsNumber;
    }


    /* package */ void resetPossibleRowsNumber() {
        possibleRowsNumber.clear();
    }

    public List<Integer> getPossibleRowNumber() {
        return possibleRowsNumber;
    }

    //this method is for testing purpose only
    /* package */ void setPossibleRowNumber(ArrayList<Integer> possibleRowNumber) {
        this.possibleRowsNumber = possibleRowNumber;
    }

    /* package */ boolean isCorrectNumberOfStraightEdges(int rowSize) {

        int colSize = pieces.size() / rowSize;

        if (pieces.stream().filter(piece -> piece.getEdge(EDGE.LEFT) == 0).count() < rowSize ||
                pieces.stream().filter(piece -> piece.getEdge(EDGE.RIGHT) == 0).count() < rowSize ||
                pieces.stream().filter(piece -> piece.getEdge(EDGE.TOP) == 0).count() < colSize ||
                pieces.stream().filter(piece -> piece.getEdge(EDGE.BOTTOM) == 0).count() < colSize) {
            return false;
        }
        this.errorsManager.removeError(Error.MATCHING_STRAIGHT);
        return true;
    }

    /* package */ Set<String> getMissingCorners(int rowSize) {

        Set<String> missingCorners = new HashSet<>();

        int colSize = pieces.size() / rowSize;

        Set<Piece> cornerTL = pieces.stream()
                .filter(piece -> piece.getEdge(EDGE.TOP) == 0)
                .filter(piece -> piece.getEdge(EDGE.LEFT) == 0)
                .collect(Collectors.toSet());
        Set<Piece> cornerTR = pieces.stream()
                .filter(piece -> piece.getEdge(EDGE.TOP) == 0)
                .filter(piece -> piece.getEdge(EDGE.RIGHT) == 0)
                .collect(Collectors.toSet());
        Set<Piece> cornerBL = pieces.stream()
                .filter(piece -> piece.getEdge(EDGE.BOTTOM) == 0)
                .filter(piece -> piece.getEdge(EDGE.LEFT) == 0)
                .collect(Collectors.toSet());
        Set<Piece> cornerBR = pieces.stream()
                .filter(piece -> piece.getEdge(EDGE.BOTTOM) == 0)
                .filter(piece -> piece.getEdge(EDGE.RIGHT) == 0)
                .collect(Collectors.toSet());

        // if rowsize or colsize =1 then should be 2 "full corner" pieces as shape П; else - 4 "regular" as ᒥ
        if (rowSize > 1 && colSize > 1) {

        //for all cases below - need to exclude "full corner" pieces as shape П

            cornerTL.removeAll(cornerTR);
            cornerTL.removeAll(cornerBL);
            if (cornerTL.isEmpty()) {
                missingCorners.add(TL);
            }

            cornerBL.removeAll(cornerTL);
            cornerBL.removeAll(cornerBR);
            if (cornerBL.isEmpty()) {
                missingCorners.add(BL);
            }

            cornerTR.removeAll(cornerTL);
            cornerTR.removeAll(cornerBR);
            if (cornerTR.isEmpty()) {
                missingCorners.add(TR);
            }

            cornerBR.removeAll(cornerTR);
            cornerBR.removeAll(cornerBL);
            if (cornerBR.isEmpty()) {
                missingCorners.add(BR);
            }
        }
        else if (rowSize == 1) {
            //in this case - need to exclude "regular" pieces as shape ᒥ

            cornerTL.retainAll(cornerBL);
            if (cornerTL.isEmpty()) {
                missingCorners.add(TL);
                missingCorners.add(BL);
            }
            cornerBR.retainAll(cornerTR);
            if (cornerBR.isEmpty()) {
                missingCorners.add(TR);
                missingCorners.add(BR);
            }
        }

        else if (colSize == 1) {
            cornerTL.retainAll(cornerTR);
            if (cornerTL.isEmpty()) {
                missingCorners.add(TL);
                missingCorners.add(TR);
            }
            cornerBL.retainAll(cornerBR);
            if (cornerBR.isEmpty()) {
                missingCorners.add(BL);
                missingCorners.add(BR);
            }
        }
        return missingCorners;
    }

    /* package */
    void checkSumOfEdgesIsZero() {

        int sumLR = pieces.stream().mapToInt(piece -> piece.getEdge(EDGE.LEFT)).sum() +
                pieces.stream().mapToInt(piece -> piece.getEdge(EDGE.RIGHT)).sum();
        int sumTB = pieces.stream().mapToInt(piece -> piece.getEdge(EDGE.TOP)).sum() +
                pieces.stream().mapToInt(piece -> piece.getEdge(EDGE.BOTTOM)).sum();

        if (sumLR != 0 || sumTB != 0) {
        	errorsManager.addError(Error.SUM_NOT_ZERO);
        }
    }
}