package boost.puzzle.server;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import boost.puzzle.models.Piece;
import boost.puzzle.models.Position;

public class Solution {

    private List<Piece> solutionPieces;
    private Position nextPiecePosition = new Position(0, 0);
    private int rows;
    private transient int cols;

    public Solution(List<Piece> solutionPieces) {
        this.solutionPieces = solutionPieces;
    }

    public Solution(Solution solution) {
        this.solutionPieces = new ArrayList<>(solution.getSolutionPieces());
        this.rows = solution.getRows();
        this.cols = solution.getCols();
    }

    public void setSizes(int rowSize, int piecesNumber) {
        this.rows = rowSize;
        this.cols = piecesNumber / rowSize;
    }

    /*for testing purpose only*/
    public void setSolutionPieces(List<Piece> solutionPieces) {
        this.solutionPieces = solutionPieces;
    }

    /*for testing purpose only*/
    public void setNextPiecePosition(Position nextPiecePosition) {
        this.nextPiecePosition = nextPiecePosition;
    }

    public List<Piece> getSolutionPieces() {
        return solutionPieces;
    }

    public List<Integer> getSolutionIds() {
        return getSolutionPieces().stream().map(Piece::getId).collect(Collectors.toList());
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return this.cols;
    }

    public void add(Piece piece) {
        this.solutionPieces.add(piece);
        int row = solutionPieces.size() / getCols();
        int col = solutionPieces.size() % getCols();
        nextPiecePosition = new Position(row, col);
    }

    public Piece getLeftPiece() {
        if (nextPiecePosition.getCol() > 0) {
            int index = nextPiecePosition.getRow() * getCols() + nextPiecePosition.getCol() - 1;
            return solutionPieces.get(index);
        }
        return null;
    }

    public Piece getTopPiece() {
        if (nextPiecePosition.getRow() > 0) {
            int index = (nextPiecePosition.getRow() - 1) * getCols() + nextPiecePosition.getCol();
            return solutionPieces.get(index);
        }
        return null;
    }

    public Pattern getPatternForNextPiece() {
        int right = 0;
        int bottom = 0;
        if (!(getLeftPiece() == null)) {
            right = getLeftPiece().getEdge(Pattern.EDGE.RIGHT);
        }
        if (!(getTopPiece() == null)) {
            bottom = getTopPiece().getEdge(Pattern.EDGE.BOTTOM);
        }

        return new Pattern(new int[]{-right, -bottom});
    }


    boolean doesMatch(Piece piece) {
        // check that for exterior row/col must have 0 from the relevant side

        if (nextPiecePosition.getRow() == 0 && piece.getEdge(Pattern.EDGE.TOP) != 0) {
            return false;
        } else if (nextPiecePosition.getCol() == 0 && piece.getEdge(Pattern.EDGE.LEFT) != 0) {
            return false;
        } else if (nextPiecePosition.getRow() == getCols() - 1 && piece.getEdge(Pattern.EDGE.BOTTOM) != 0) {
            return false;
        } else if (nextPiecePosition.getCol() == getCols() - 1 && piece.getEdge(Pattern.EDGE.RIGHT) != 0) {
            return false;

            // must match with the neighbors from the left and top
        } else if (nextPiecePosition.getCol() > 0 && piece.getEdge(Pattern.EDGE.LEFT)
                + getLeftPiece().getEdge(Pattern.EDGE.RIGHT) != 0) {
            return false;
        } else if (nextPiecePosition.getRow() > 0 && piece.getEdge(Pattern.EDGE.TOP)
                + getTopPiece().getEdge(Pattern.EDGE.BOTTOM) != 0) {
            return false;
        }
        return true;
    }

    public boolean isEmpty() {
        return solutionPieces.isEmpty();
    }

//    Delete the method if it remains unusable until 30.06.18
//    Israel Kliger
//
//    public Piece getPieceById(int i) {
//        return solutionPieces.stream().filter(p -> p.getId() == i).findFirst().orElse(null);
//    }

    //this method is for SolutionChecker only
    public Piece getPieceBySolutionIndex(int i) {
        return getSolutionPieces().get(i);
    }

    public boolean doesMatchIndexed(Piece piece) {
        // check that for exterior row/col must have 0 from the relevant side

        if (nextPiecePosition.getRow() == rows - 1 && piece.getEdge(Pattern.EDGE.BOTTOM) != 0) {
            return false;
        } else if (nextPiecePosition.getCol() == getCols() - 1 && piece.getEdge(Pattern.EDGE.RIGHT) != 0) {
            return false;
        }
        return true;
    }
}