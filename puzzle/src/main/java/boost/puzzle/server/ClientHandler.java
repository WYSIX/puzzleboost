package boost.puzzle.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import boost.puzzle.consts.ConfigurationConsts;
import boost.puzzle.models.Piece;
import boost.puzzle.models.Puzzle;
import boost.puzzle.models.PuzzleReceived;
import boost.puzzle.models.RequestWrapper;
import boost.puzzle.utils.JsonUtils;

public class ClientHandler implements Runnable {

	private Socket socket;
	private int sessionId;

	public ClientHandler(Socket socket, int sessionId) {
		this.socket = socket;
		this.sessionId = sessionId;
	}

	@Override
	public void run() {
		System.out.println("Running a new client with session id: " + sessionId);
		
		Solver puzzleSolver = new Solver();
		try (DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream());
		        DataInputStream dataInputStream = new DataInputStream(socket.getInputStream())) {

			// Receive pieces as a JSON string from client
			String jsonInputFromClient = dataInputStream.readUTF();
			System.out.println("The following client input has been recieved:\n" + jsonInputFromClient);

			// Convert JSON string to list of pieces
			JsonParser parser = new JsonParser();
			JsonObject o = parser.parse(jsonInputFromClient).getAsJsonObject();
			List<Piece> pieces = convertJsonElementToListOfPieces(o);

			PuzzleReceived puzzleReceived = new PuzzleReceived(sessionId, pieces.size());
			String objectAsJsonString = JsonUtils.toJsonStringWrappedWithRoot(puzzleReceived, ConfigurationConsts.WRAPPER_PUZZLE_RECEIVED);
			dataOutputStream.writeUTF(objectAsJsonString);

			JsonElement responseJson = puzzleSolver.start(pieces);

			// Send solver response as a JSON string back to the client
			dataOutputStream.writeUTF(responseJson.toString());
			System.out.println("The following response has been sent back to the client:\n" + responseJson.toString());
		} catch (

		IOException e) {
			e.printStackTrace();
		}
	}

	private List<Piece> convertJsonElementToListOfPieces(JsonElement jsonRequest) {
		Gson gson = new Gson();
		RequestWrapper requestWrapper = gson.fromJson(jsonRequest, RequestWrapper.class);
		Puzzle puzzle = requestWrapper.getPuzzle();
		return puzzle.getPieces();
	}

}
