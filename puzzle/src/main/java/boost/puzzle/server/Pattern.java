package boost.puzzle.server;

import com.google.gson.annotations.Expose;

import java.util.Arrays;

public class Pattern {

    @Expose(serialize = false, deserialize = false)
    private int[] piece;

    public Pattern(int[] piece) {
        this.piece = piece;
    }

    //TODO: remove unused methods
    //up to 30.06.18

    public int[] getPiece() {
        return piece;
    }

    public int getEdge(EDGE edge) {
        return piece[edge.index];
    }

    public void setPiece(int[] piece) {
        this.piece = piece;
    }

    public enum EDGE {
        LEFT(0), TOP(1), RIGHT(2), BOTTOM(3);

        private int index;

        private EDGE(int index) {
            this.index = index;
        }

        public int index() {
            return this.index;
        }
    }

    public int getTopLeftPattern() {
        return hashCode();
    }

    @Override
    public int hashCode() {

//      indexing is by left and top piece only
        int hashcode = 10 * piece[0] + piece[1];
        return hashcode;
    }

    @Override
    public boolean equals(Object o) {
        return Arrays.equals(((Pattern) o).piece, this.piece);
    }
}
