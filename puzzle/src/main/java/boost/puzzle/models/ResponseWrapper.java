package boost.puzzle.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseWrapper {

    @SerializedName("puzzleSolution")
    @Expose private Response response;

    public ResponseWrapper(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    public JsonElement serialize() {
        Gson gson = new Gson();
        return gson.toJsonTree(this);
    }

    public JsonElement serializeExpose() {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
        return gson.toJsonTree(this);
    }
}
