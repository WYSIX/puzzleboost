package boost.puzzle.models;

public class PuzzleReceived {

	private int sessionId;
	private int numPieces;

	public PuzzleReceived(int sessionId, int numPieces) {
		this.sessionId = sessionId;
		this.numPieces = numPieces;
	}

	public int getSessionId() {
		return sessionId;
	}

	public int getNumPieces() {
		return numPieces;
	}

}
