package boost.puzzle.models;

import java.util.List;

public class Puzzle {
    List<Piece> pieces;

	public Puzzle(List<Piece> pieces) {
        this.pieces = pieces;
    }

    public List<Piece> getPieces() {
        return pieces;
    }
}
