package boost.puzzle.models;

import com.google.gson.annotations.Expose;

import boost.puzzle.server.Solution;

import java.util.List;
import java.util.stream.Collectors;

public class SolutionWrapper {

    @Expose private int rows;
    @Expose private List<Integer> solutionPieces;

    public SolutionWrapper(Solution successSolution) {
        this.rows = successSolution.getRows();
        this.solutionPieces = successSolution.getSolutionPieces().stream().map(Piece::getId).collect(Collectors.toList());
    }

    public List<Integer> getSolution() {
        return solutionPieces;
    }

    public int getRows() {
        return rows;
    }
}