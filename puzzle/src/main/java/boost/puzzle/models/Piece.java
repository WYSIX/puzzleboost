package boost.puzzle.models;

import boost.puzzle.server.Pattern;

public class Piece extends Pattern implements Comparable<Piece> {

	private int id;

	public Piece(int id, int[] edges) {
		super(edges);
		this.id = id;
	}

	public Piece(int id, int left, int top, int right, int bottom) {
		super(new int[] { left, top, right, bottom });
		this.id = id;
	}

	public int getId() {
		return id;
	}

	@Override
	public int compareTo(Piece other) {
		if (this.id > other.id) {
			return 1;
		} else if (this.id < other.id) {
			return -1;
		} else {
			return 0;
		}
	}
}