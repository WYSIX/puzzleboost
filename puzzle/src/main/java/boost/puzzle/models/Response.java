package boost.puzzle.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import boost.puzzle.ErrorsManager;

import java.util.List;

public class Response {

    @SerializedName("solution")
    @Expose
    private SolutionWrapper solutionWrapper;
    @Expose
    private List<String> errors;
    @Expose(serialize = false)
    private boolean solutionExists = true;

    public List<String> getErrors() {
        return errors;
    }

    public boolean hasErrors() {
        return errors != null;
    }

    public void addSolutionWrapper(SolutionWrapper solutionWrapper) {
        this.solutionWrapper = solutionWrapper;
    }

    public SolutionWrapper getSolutionWrapper() {
        return solutionWrapper;
    }

    public void setSolutionExistsFalse() {
        solutionExists = false;
    }

    public boolean doesSolutionExist() {
        return solutionExists;
    }

    public void addErrorsManager(ErrorsManager errorsManager) {
        this.errors = errorsManager.getErrorsMap();
    }
}
