package boost.puzzle.models;

import com.google.gson.annotations.SerializedName;

public class RequestWrapper {

    @SerializedName("puzzle")
    private Puzzle puzzle;

    public Puzzle getPuzzle() {
        return puzzle;
    }

    public RequestWrapper(Puzzle puzzle) {
        this.puzzle = puzzle;
    }
}
