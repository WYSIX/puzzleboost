package boost.puzzle;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import boost.puzzle.consts.Error;

public class ErrorsManager {

	private Map<Error, List<String>> errorsMap = new TreeMap<>();
	private transient Set<Integer> duplicateIds = new TreeSet<>();
	private transient List<Integer> wrongIds = new ArrayList<>();
	private transient List<Integer> missingIds = new ArrayList<>();

	public List<String> getErrorsMap() {
		return errorsMap.values().stream().flatMap(List::stream).collect(Collectors.toList());
	}

	public String getWrongIds() {
		return wrongIds.stream().map(id -> id.toString()).collect(Collectors.joining(", "));
	}

	public String getMissingIds() {
		return missingIds.stream().map(id -> id.toString()).collect(Collectors.joining(", "));
	}

	public Set<Integer> getDuplicateIds() {
		return duplicateIds;
	}

	public void addError(Error error, Object... params) {
		List<String> currentList = errorsMap.getOrDefault(error, new ArrayList<>());
		currentList.add(String.format(error.getMessage(), params));
		errorsMap.put(error, currentList);
	}

	public void removeError(Error error) {
		errorsMap.remove(error);
	}

	public void addWrongId(Integer id) {
		wrongIds.add(id);
	}

	public void addMissingId(Integer id) {
		missingIds.add(id);
	}

	public void addDuplicateId(Integer id) {
		duplicateIds.add(id);
	}

	public boolean isEmpty() {
		return errorsMap.isEmpty();
	}

}
