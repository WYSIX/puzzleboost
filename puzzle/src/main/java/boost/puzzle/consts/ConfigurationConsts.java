package boost.puzzle.consts;

public class ConfigurationConsts {

	public static final String INPUT_FILES_PATH = "src/resources/Files/";
	public static final String TEST_FILES_PATH = "src/resources/TestFiles/";
	public static final String FILE_ENCODING = "UTF8";

	public static final int DEFAULT_THREADS_NUMBER = 3;
	public static final int DEFAULT_PORT_NUMBER = 7095;
	public static final String DEFAULT_SERVER_ADDRESS = "127.0.0.1";

	public static final String COMMENT_IDENTIFIER = "#";
	public static final String NUM_ELEMENTS = "NumElements";
	public static final String EQUALS_SIGN = "=";
	public static final String REGEX_ANY_WHITE_SPACES = "\\s+";

	public static final int EDGE_EXTERIOR = 1;
	public static final int EDGE_INTERIOR = -1;
	public static final int EDGE_STRAIGHT = 0;
	public static final int NUMBER_OF_VALUES_PER_PIECE = 5;
	public static final int MINIMUM_PIECE_ID = 1;
	
	public static final String WRAPPER_PUZZLE_RECEIVED = "PuzzleReceived";

	public enum CONSOLE_PARAMS_SERVER {
		PORT("-port"),
		THREADS("-threads");

		public static final int MAXIMUM_CONSOLE_ARGS = 4;
		public static final int MINIMUM_CONSOLE_ARGS_WHEN_PRESENT = 2;

		private String name;

		private CONSOLE_PARAMS_SERVER(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}

	}

	public enum CONSOLE_PARAMS_CLIENT {

	}

}
