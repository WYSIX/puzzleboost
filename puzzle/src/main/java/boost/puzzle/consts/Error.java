package boost.puzzle.consts;

	//The natural order implemented by this method is the order in which the constants are declared.
	public enum Error {
		
		MISSING_ELEMENT("Missing puzzle element(s) with the following IDs: %s"),
		ID_EXEEDS_SIZE("Puzzle of size <%s> cannot have the following IDs: %s"),
		WRONG_ELEMENT_FORMAT("Puzzle ID <%s> has wrong data: <%s>"),
		MATCHING_STRAIGHT ("Cannot solve puzzle: wrong number of straight edges"),
		MISSING_CORNER ("Cannot solve puzzle: missing corner element: %s"), // <TL><TR><BL><BR> list all missing corners, each with its own line in the order
		SUM_NOT_ZERO("Cannot solve puzzle: sum of edges is not zero"),
		COULD_NOT_READ_FILE("Failed to read file, return error was <%s>"),
		FIRST_LINE_BEFORE_SIGN("First line must start with <NumElements>, instead was <%s>"),
		FIRST_LINE_AFTER_SIGN("First line must end with an integer representing the number of elements, instead was <%s>"),
		PIECE_VALUE_NOT_NUMERIC("All piece values must be numeric, instead got <%s> on line <%s>"),
		DUPLICATE_IDS("Duplicate IDS found in the input file, <%s>"),
		BAD_PARAMETERS_SERVER("Bad parametrs detected, must be -threads <num_threads> (optional) -port <port_number> (optional)"),
		BAD_PARAMETERS_CLIENT("Bad parametrs detected, must be -ip <ip_address> (optional) -port <port_number> (optional), -output <output_file_name> (Mandatory)"), 
		NO_SOLUTION ("Cannot solve puzzle: it seems that there is no proper solution"); //never come with any other error, it's the last one

		private String message;
		
		private Error(String message) {
			this.message = message;
		}
		
		public String getMessage() {
			return message;
		}
		
	}
