package boost.puzzle.utils;

import java.util.ArrayList;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import boost.puzzle.models.Piece;
import boost.puzzle.server.Solution;

class SolutionCheckerTest {
    //SolutionChecker is utility for check of found solutions

    @Test
    void testIsSolutionGoodPositive() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(5, new int[]{0,0,1,1}));
        pieces.add(new Piece(1, new int[]{-1,0,-1,0}));
        pieces.add(new Piece(3, new int[]{1,0,0,-1}));
        pieces.add(new Piece(2, new int[]{0,-1,-1,0}));
        pieces.add(new Piece(6, new int[]{1,0,1,0}));
        pieces.add(new Piece(4, new int[]{-1,1,0,0}));


        Solution solution = new Solution(pieces);
        solution.setSizes(2, pieces.size());
        SolutionChecker checker = new SolutionChecker(solution);
        boolean checkResult = checker.isSolutionGood();
        Assertions.assertTrue(checkResult, "The solution should be verified");
    }

    @Test
    void testIsSolutionGoodPositive2Pieces() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{0,0,-1,0}));
        pieces.add(new Piece(2, new int[]{1,0,0,0}));


        Solution solution = new Solution(pieces);
        solution.setSizes(1, pieces.size());

        SolutionChecker checker = new SolutionChecker(solution);
        boolean checkResult = checker.isSolutionGood();
        Assertions.assertTrue(checkResult, "The solution should be verified");
    }

    @Test
    void testIsSolutionGoodNegative() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(5, new int[]{0,0,1,1}));
        pieces.add(new Piece(1, new int[]{-1,0,-1,0}));
        pieces.add(new Piece(3, new int[]{1,0,0,-1}));
        pieces.add(new Piece(2, new int[]{0,-1,-1,0}));
        pieces.add(new Piece(6, new int[]{1,0,1,0}));
        pieces.add(new Piece(4, new int[]{-1,1,0,0}));

        Solution solution = new Solution(pieces);
        solution.setSizes(1, pieces.size());

        SolutionChecker checker = new SolutionChecker(solution);
        boolean checkResult = checker.isSolutionGood();
        Assertions.assertFalse(checkResult, "The solution should NOT be verified");
    }
}