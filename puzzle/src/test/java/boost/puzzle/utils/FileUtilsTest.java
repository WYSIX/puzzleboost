package boost.puzzle.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.jupiter.api.Test;

import boost.puzzle.consts.ConfigurationConsts;
import boost.puzzle.utils.FileUtils;

class FileUtilsTest {

	@Test
	void readAndWriteTest() throws UnsupportedEncodingException, FileNotFoundException, IOException {
		Path fileToReadPath = Paths.get(ConfigurationConsts.TEST_FILES_PATH + "inputWriteReadTest.txt");
		Path fileToWritePath = Paths.get(ConfigurationConsts.TEST_FILES_PATH + "outputWriteReadTest.txt");

		List<String> linesFromFileRead = FileUtils.readTextFile(fileToReadPath, ConfigurationConsts.FILE_ENCODING);

		FileUtils.writeTextFile(fileToWritePath, ConfigurationConsts.FILE_ENCODING, linesFromFileRead);

		List<String> linesFromFileReadAfterWrite = FileUtils.readTextFile(fileToWritePath,
				ConfigurationConsts.FILE_ENCODING);

		assertThat(linesFromFileRead).isEqualTo(linesFromFileReadAfterWrite);
	}

}
