package boost.puzzle;

import static java.time.Duration.ofSeconds;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import boost.puzzle.client.GameClient;
import boost.puzzle.consts.ConfigurationConsts;
import boost.puzzle.server.GameServer;
import boost.puzzle.utils.FileUtils;

class ClientToServerTest {

	@BeforeAll
	public static void beforeAll() {
		new Thread(() -> {
			GameServer server = new GameServer();
			server.start(ConfigurationConsts.DEFAULT_THREADS_NUMBER, ConfigurationConsts.DEFAULT_PORT_NUMBER);
		}).start();
	}

	@Test
	void testPlayFoundSolution9Pieces() throws IOException {
		GameClient gameClient = new GameClient();

		String fileName = "validInputFileForTest9.txt";
		String filePath =  ConfigurationConsts.TEST_FILES_PATH + fileName;
		gameClient.start(ConfigurationConsts.DEFAULT_SERVER_ADDRESS, ConfigurationConsts.DEFAULT_PORT_NUMBER, filePath, null);

		Path path = Paths.get(ConfigurationConsts.TEST_FILES_PATH + FileUtils.generateOutputFileName(fileName));
		List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);

		assertThat(linesFromFile).hasSize(3);
		assertThat(linesFromFile.get(0)).isEqualTo("3 7 4");
		assertThat(linesFromFile.get(1)).isEqualTo("5 1 6");
		assertThat(linesFromFile.get(2)).isEqualTo("2 9 8");
	}

	@Test
	void testPlayFoundSolution12Pieces() throws IOException {
		GameClient gameClient = new GameClient();

		String fileName = "validInputFileForTest12.txt";
		String filePath =  ConfigurationConsts.TEST_FILES_PATH + fileName;
		gameClient.start(ConfigurationConsts.DEFAULT_SERVER_ADDRESS, ConfigurationConsts.DEFAULT_PORT_NUMBER, filePath, null);

		Path path = Paths.get(ConfigurationConsts.TEST_FILES_PATH + FileUtils.generateOutputFileName(fileName));
		List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);
		assertThat(linesFromFile).hasSize(3);
		assertThat(linesFromFile.get(0)).isEqualTo("1 4 9 7");
		assertThat(linesFromFile.get(1)).isEqualTo("10 8 5 6");
		assertThat(linesFromFile.get(2)).isEqualTo("3 12 11 2");
	}

	@Test
	void testPlayFoundSolution20Pieces1Row() throws IOException {
		GameClient gameClient = new GameClient();

		String fileName = "validInputFileForTest20.txt";
		String filePath =  ConfigurationConsts.TEST_FILES_PATH + fileName;

		Assertions.assertTimeout(ofSeconds(5), () -> {
			gameClient.start(ConfigurationConsts.DEFAULT_SERVER_ADDRESS, ConfigurationConsts.DEFAULT_PORT_NUMBER,
					filePath, null);
		}, "Execution time should be less than 5 sec because 'possible rows number' optimization");

		Path path = Paths.get(ConfigurationConsts.TEST_FILES_PATH + FileUtils.generateOutputFileName(fileName));
		List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);
		assertThat(linesFromFile).hasSize(1);
		assertThat(linesFromFile.get(0)).isEqualTo("4 5 6 7 9 13 1 14 15 2 17 3 18 19 10 11 16 20 8 12");
	}

//  @Test
//  void testPlayFoundSolution64Pieces() throws IOException {
//      GameManager gameManager = new GameManager();
//
//      String fileName = "validInputFileForTest64.txt";
//
//      Assertions.assertTimeout(ofSeconds(60), () -> {
//          gameManager.play(ConfigurationConsts.TEST_FILES_PATH, fileName);
//      }, "Execution time should be less than 60 sec because 'possible rows number' optimization");
//
//      Path path = Paths.get(ConfigurationConsts.OUTPUT_FILES_PATH + FileUtils.generateOutputFileName(fileName));
//      List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);
//      assertThat(linesFromFile).hasSize(8);
//  }

	@Test
	void testPlayNoSolution() throws IOException {
		GameClient gameClient = new GameClient();

		String fileName = "validInputFileNoSolutionForTest.txt";
		String filePath =  ConfigurationConsts.TEST_FILES_PATH + fileName;
		gameClient.start(ConfigurationConsts.DEFAULT_SERVER_ADDRESS, ConfigurationConsts.DEFAULT_PORT_NUMBER, filePath, null);

		Path path = Paths.get(ConfigurationConsts.TEST_FILES_PATH + FileUtils.generateOutputFileName(fileName));
		List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);

		assertThat(linesFromFile).hasSize(1);
		assertThat(linesFromFile.get(0)).isEqualTo(boost.puzzle.consts.Error.NO_SOLUTION.getMessage());
	}

	@Test
	void testPlaySumIsZero() throws IOException {
		GameClient gameClient = new GameClient();

		String fileName = "validInputFileSumIsZero.txt";
		String filePath =  ConfigurationConsts.TEST_FILES_PATH + fileName;
		gameClient.start(ConfigurationConsts.DEFAULT_SERVER_ADDRESS, ConfigurationConsts.DEFAULT_PORT_NUMBER, filePath, null);

		Path path = Paths.get(ConfigurationConsts.TEST_FILES_PATH + FileUtils.generateOutputFileName(fileName));
		List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);

		assertThat(linesFromFile).hasSize(1);
		assertThat(linesFromFile.get(0)).isEqualTo(boost.puzzle.consts.Error.SUM_NOT_ZERO.getMessage());
	}

	@Test
	void testSomeErrorsStraighEdgesAnd2MissingCorners() throws IOException {

		GameClient gameClient = new GameClient();

		String fileName = "validInputStraightEdgesErrorAnd2MissingCorners.txt";
		String filePath =  ConfigurationConsts.TEST_FILES_PATH + fileName;
		gameClient.start(ConfigurationConsts.DEFAULT_SERVER_ADDRESS, ConfigurationConsts.DEFAULT_PORT_NUMBER, filePath, null);

		Path path = Paths.get(ConfigurationConsts.TEST_FILES_PATH + FileUtils.generateOutputFileName(fileName));
		List<String> linesFromFile = FileUtils.readTextFile(path, ConfigurationConsts.FILE_ENCODING);

		List<String> expectedCornerErrors = Arrays.asList("wrong number of straight edges", "TL", "BR");
		assertThat(linesFromFile).hasSize(3);
		assertThat(linesFromFile.get(0)).isEqualTo(boost.puzzle.consts.Error.MATCHING_STRAIGHT.getMessage());

		for (int i = 0; i < 2; i++) {
			assertThat(linesFromFile.get(i)).contains(expectedCornerErrors.get(i));
		}
	}

}
