package boost.puzzle.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import boost.puzzle.models.Piece;
import boost.puzzle.server.Solver;
import boost.puzzle.utils.SolutionChecker;

import java.util.ArrayList;
import java.util.Arrays;

class SolverTest {

    @Test
    void testSolve1Piece() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{0, 0, 0, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be: (1 row) 1");
    }

    @Test
    void testSolve4Pieces2Rows() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1, 0, 0, 1}));
        pieces.add(new Piece(2, new int[]{0, -1, 0, 0}));
        pieces.add(new Piece(3, new int[]{0, 0, 1, -1}));
        pieces.add(new Piece(4, new int[]{0, 1, 0, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");

    }


    @Test
    void testSolve2Pieces1Column() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{0, 0, 0, 1}));
        pieces.add(new Piece(2, new int[]{0, -1, 0, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");

    }

    @Test
    void testSolve4Pieces1Column() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{0, 1, 0, 1}));
        pieces.add(new Piece(2, new int[]{0, 0, 0, 0}));
        pieces.add(new Piece(3, new int[]{0, 0, 0, -1}));
        pieces.add(new Piece(4, new int[]{0, -1, 0, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");

    }

    @Test
    void testSolve4Pieces1Row() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1, 0, -1, 0}));
        pieces.add(new Piece(2, new int[]{0, 0, 0, 0}));
        pieces.add(new Piece(3, new int[]{0, 0, 1, 0}));
        pieces.add(new Piece(4, new int[]{1, 0, 0, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");

    }

    @Test
    void testSolve6Pieces2Rows() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1, 0, -1, 0}));
        pieces.add(new Piece(2, new int[]{0, -1, -1, 0}));
        pieces.add(new Piece(3, new int[]{1, 0, 0, -1}));
        pieces.add(new Piece(4, new int[]{-1, 1, 0, 0}));
        pieces.add(new Piece(5, new int[]{0, 0, 1, 1}));
        pieces.add(new Piece(6, new int[]{1, 0, 1, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 3, 6)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");
    }

    @Test
    void testSolve9Pieces3Rows() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(9, new int[]{1, -1, -1, 0}));
        pieces.add(new Piece(1, new int[]{-1, -1, -1, 1}));
        pieces.add(new Piece(2, new int[]{0, 1, -1, 0}));
        pieces.add(new Piece(3, new int[]{0, 0, 0, 1}));
        pieces.add(new Piece(4, new int[]{1, 0, 0, -1}));
        pieces.add(new Piece(5, new int[]{0, -1, 1, -1}));
        pieces.add(new Piece(6, new int[]{1, 1, 0, 0}));
        pieces.add(new Piece(7, new int[]{0, 0, -1, 1}));
        pieces.add(new Piece(8, new int[]{1, 0, 0, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 3, 9)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");
    }


    @Test
    void testSolve4Pieces2RowsManyZeros() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1, 1, 0, 0}));
        pieces.add(new Piece(2, new int[]{0, -1, 1, 0}));
        pieces.add(new Piece(3, new int[]{0, 0, 0, -1}));
        pieces.add(new Piece(4, new int[]{0, 0, 0, 1}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");
    }

    @Test
    void testSolve8Pieces2Rows() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{1, 1, 0, 0}));
        pieces.add(new Piece(2, new int[]{0, 0, 0, -1}));
        pieces.add(new Piece(3, new int[]{0, 1, 0, 0}));
        pieces.add(new Piece(4, new int[]{0, 0, 0, 1}));
        pieces.add(new Piece(5, new int[]{-1, 0, 0, 0}));
        pieces.add(new Piece(6, new int[]{0, -1, 0, 0}));
        pieces.add(new Piece(7, new int[]{0, 0, -1, 0}));
        pieces.add(new Piece(8, new int[]{0, 0, 1, -1}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 4, 8)));
        Assertions.assertTrue(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        SolutionChecker checker = new SolutionChecker(puzzleSolver.getSolution());
        Assertions.assertTrue(checker.isSolutionGood(), "The puzzle solution should be validated by SolutionChecker");
    }

    @Test
    void testNegativeNoSolution() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1, 0, -1, 0}));
        pieces.add(new Piece(2, new int[]{0, -1, -1, 0}));
        pieces.add(new Piece(3, new int[]{1, 0, 0, -1}));
        pieces.add(new Piece(4, new int[]{-1, 1, 0, 0}));
        pieces.add(new Piece(5, new int[]{0, 0, 1, 1}));
        pieces.add(new Piece(6, new int[]{-1, 0, 1, 0}));

        Solver puzzleSolver = new Solver();
        puzzleSolver.setPieces(pieces);
        puzzleSolver.setValidatedRowsNumber(new ArrayList<>(Arrays.asList(1, 2, 3, 6)));
        Assertions.assertFalse(puzzleSolver.solveAnySize(), "The puzzle should be solved");
        Assertions.assertTrue(puzzleSolver.getSolution().isEmpty(), "The puzzle board (solution) should be empty");

    }
}