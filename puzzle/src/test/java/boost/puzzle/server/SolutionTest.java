package boost.puzzle.server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import boost.puzzle.client.GameClient;
import boost.puzzle.models.Piece;
import boost.puzzle.models.Position;
import boost.puzzle.models.SolutionWrapper;
import boost.puzzle.server.Pattern;
import boost.puzzle.server.Solution;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class SolutionTest {

    @Test
    void testAddPieceToSolution() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1,0,-1,0}));
        pieces.add(new Piece(4, new int[]{-1,1,0,0}));
        pieces.add(new Piece(2, new int[]{0,-1,-1,0}));
        pieces.add(new Piece(3, new int[]{1,0,0,-1}));
        pieces.add(new Piece(5, new int[]{0,0,1,1}));
        Piece pieceToAdd = new Piece(6, new int[]{1,0,1,0});

        Solution solution = new Solution(pieces);
        solution.setSizes(2, pieces.size());

        solution.setNextPiecePosition(new Position(1,2));
        solution.add(pieceToAdd);
        Assertions.assertEquals(solution.getPieceBySolutionIndex(5), pieceToAdd, "Piece should be added correctly");
    }

    @Test
    void testGetTopPiece() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1,0,-1,0}));
        pieces.add(new Piece(4, new int[]{-1,1,0,0}));
        pieces.add(new Piece(2, new int[]{0,-1,-1,0}));
        pieces.add(new Piece(3, new int[]{1,0,0,-1}));
        pieces.add(new Piece(5, new int[]{0,0,1,1}));
        pieces.add(new Piece(6, new int[]{1,0,1,0}));

        Solution solution = new Solution(pieces);
        solution.setSizes(2, pieces.size());

        solution.setNextPiecePosition(new Position(1,2));
        int expected = 2;

        Assertions.assertEquals(expected, solution.getTopPiece().getId(),
                "Top piece ID should be 2");
    }

    @Test
    void testGetLeftPiece() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1,0,-1,0}));
        pieces.add(new Piece(4, new int[]{-1,1,0,0}));
        pieces.add(new Piece(2, new int[]{0,-1,-1,0}));
        pieces.add(new Piece(3, new int[]{1,0,0,-1}));
        pieces.add(new Piece(5, new int[]{0,0,1,1}));
        pieces.add(new Piece(6, new int[]{1,0,1,0}));

        Solution solution = new Solution(pieces);
        solution.setSizes(2, pieces.size());

        solution.setNextPiecePosition(new Position(1,2));
        int expected = 5;

        Assertions.assertEquals(expected, solution.getLeftPiece().getId(),
                "Left piece ID should be 5");
    }

    @Test
    void testGetPatternForNextPiece() {

        Solution solution = new Solution(new ArrayList<>());
        solution.setSizes(1, 2);

        solution.add(new Piece(1, new int[]{-1,0,-1,0}));

        Pattern expectedPattern = new Pattern (new int[]{1, 0});

        Assertions.assertEquals(solution.getPatternForNextPiece(),expectedPattern,
                "Pattern for the next piece should be {1, 0}");
    }

    @Test
    void generateSolutionPrintout() {

        ArrayList<Piece> pieces = new ArrayList<>();
        pieces.add(new Piece(1, new int[]{-1,0,-1,0}));
        pieces.add(new Piece(4, new int[]{-1,1,0,0}));
        pieces.add(new Piece(2, new int[]{0,-1,-1,0}));
        pieces.add(new Piece(3, new int[]{1,0,0,-1}));
        pieces.add(new Piece(5, new int[]{0,0,1,1}));
        pieces.add(new Piece(6, new int[]{1,0,1,0}));

        Solution solution = new Solution(pieces);
        solution.setSizes(2, pieces.size());
        SolutionWrapper solutionWrapper = new SolutionWrapper(solution);

        List<String> result = GameClient.generateSolutionPrintout(solutionWrapper);

        assertThat(result).hasSize(2);
        assertThat(result.get(0)).isEqualTo("1 4 2");
        assertThat(result.get(1)).isEqualTo("3 5 6");
    }
}