package boost.puzzle.server;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import boost.puzzle.ErrorsManager;
import boost.puzzle.consts.Error;
import boost.puzzle.models.Piece;
import boost.puzzle.server.PiecesValidator;

class PiecesValidatorTest {

	private ErrorsManager errorsManager = new ErrorsManager();

	@Injectable
	List<Piece> pieces;
	@Tested
	PiecesValidator validator = new PiecesValidator(pieces, errorsManager);

	@BeforeEach
	void beforeEach() {
		errorsManager = new ErrorsManager();
	}

	@Test
	void testCalcPossibleRowSizes4() {
		validator.resetPossibleRowsNumber();

		new Expectations() {
			{
				pieces.size();
				result = 4;
			}
		};
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(2, 1, 4));

		validator.calcPossibleRowsNumber();
		Assertions.assertEquals(expected, validator.getPossibleRowNumber(),
				"For 4 pieces possibleRowNumber should be 2,1,4");
	}

	@Test
	void testCalcPossibleRowSizes100() {
		validator.resetPossibleRowsNumber();

		new Expectations() {
			{
				pieces.size();
				result = 100;
			}
		};
		ArrayList<Integer> expected = new ArrayList<>(Arrays.asList(10, 5, 20, 4, 25, 2, 50, 1, 100));

		validator.calcPossibleRowsNumber();
		Assertions.assertEquals(expected, validator.getPossibleRowNumber(),
				"For 100 pieces possibleRowNumber should be 10, 5, 20, 4, 25, 2, 50, 1, 100");
	}

	@Test
	void testFilterPossibleRowSize() {
		ArrayList<Piece> pieces = new ArrayList<>();
		pieces.add(new Piece(1, new int[] { -1, 1, -1, 1 }));
		pieces.add(new Piece(2, new int[] { 0, -1, 1, -1 }));
		pieces.add(new Piece(3, new int[] { 1, -1, 1, -1 }));
		pieces.add(new Piece(4, new int[] { 1, 1, -1, 0 }));

		PiecesValidator validator = new PiecesValidator(pieces, errorsManager);
		validator.setPossibleRowNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
		validator.validatePossibleRowsNumber();
		assertThat(errorsManager.getErrorsMap().get(0)).isEqualTo(Error.MATCHING_STRAIGHT.getMessage());
	}

	@Test
	void testFilterPossibleRowSizeMissingCornerElements() {
		ArrayList<Piece> pieces = new ArrayList<>();
		pieces.add(new Piece(1, new int[] { 0, 1, 0, 1 }));
		pieces.add(new Piece(2, new int[] { 0, -1, 0, -1 }));
		pieces.add(new Piece(3, new int[] { 1, 0, 1, 0 }));
		pieces.add(new Piece(4, new int[] { 1, 0, -1, 0 }));

		PiecesValidator validator = new PiecesValidator(pieces, errorsManager);
		validator.setPossibleRowNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
		validator.validatePossibleRowsNumber();
		List<String> expectedCornerErrors = Arrays.asList("TL", "TR", "BL", "BR");
		assertThat(errorsManager.getErrorsMap()).hasSize(4);

		for (int i = 0; i < 4; i++) {
			assertThat(errorsManager.getErrorsMap().get(i)).contains(expectedCornerErrors.get(i));
		}
	}

	@Test
	void testFilterPossibleRowSizeMissing2CornersAndStraightEdges() {
		ArrayList<Piece> pieces = new ArrayList<>();
		pieces.add(new Piece(1, new int[] { -1, 1, 1, -1 }));
		pieces.add(new Piece(2, new int[] { -1, 0, 0, 1 }));
		pieces.add(new Piece(3, new int[] { 0, 1, -1, 0 }));
		pieces.add(new Piece(4, new int[] { 1, -1, 1, -1 }));

		PiecesValidator validator = new PiecesValidator(pieces, errorsManager);
		validator.setPossibleRowNumber(new ArrayList<>(Arrays.asList(1, 2, 4)));
		validator.validatePossibleRowsNumber();
		List<String> expectedCornerErrors = Arrays.asList("wrong number of straight edges", "TL", "BR");
		assertThat(errorsManager.getErrorsMap()).hasSize(3);

		for (int i = 0; i < 3; i++) {
			assertThat(errorsManager.getErrorsMap().get(i)).contains(expectedCornerErrors.get(i));
		}
	}

	@Test
	void testCheckSumOfEdgesIsZero() {
		ArrayList<Piece> pieces = new ArrayList<>();
		pieces.add(new Piece(1, new int[] { 0, 1, 0, -1 }));
		pieces.add(new Piece(2, new int[] { -1, 0, 1, 0 }));
		pieces.add(new Piece(3, new int[] { 0, 1, 0, -1 }));
		pieces.add(new Piece(4, new int[] { 1, 0, 1, 0 }));

		PiecesValidator validator = new PiecesValidator(pieces, errorsManager);
		validator.checkSumOfEdgesIsZero();
		assertThat(errorsManager.getErrorsMap().get(0)).isEqualTo(Error.SUM_NOT_ZERO.getMessage());
	}
}