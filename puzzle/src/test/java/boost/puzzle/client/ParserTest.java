package boost.puzzle.client;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import boost.puzzle.ErrorsManager;
import boost.puzzle.client.Parser;
import boost.puzzle.models.Piece;

class ParserTest {

	private static final String NUM_ELEMENTS_2 = "NumElements=2";
	private List<String> linesFromFile = new ArrayList<>();
	private Parser parser;
	private ErrorsManager errorsManager;

	@BeforeEach
	void setUp() throws Exception {
		linesFromFile.clear();
		errorsManager = new ErrorsManager();
	}

	@ParameterizedTest
	@CsvSource({ "1 0 1 0 1, 2 1 -1 1 -1", "2 0 1 0 1,1 1 -1 1 -1" })
	void testParsePositiveNoErrors(String line1, String line2) {
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add(line1);
		linesFromFile.add(line2);

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();
		
		assertThat((errorsManager.getErrorsMap())).isEmpty();
	}

	@Test
	void testParsePositiveLeadingSpacesNoErrors() {
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add(" 1 0 1 0 1");
		linesFromFile.add("  2 1 -1 1 -1");

		parser = new Parser(linesFromFile, errorsManager);
		List<Piece> pieces = parser.parse();
		
		Piece piece1 = new Piece(1, 0, 1, 0, 1);
		Piece piece2 = new Piece(2, 1, -1, 1, -1);

		assertThat((errorsManager.getErrorsMap())).isEmpty();
		assertThat(pieces.get(0)).isEqualToComparingFieldByField(piece1);
		assertThat(pieces.get(1)).isEqualToComparingFieldByField(piece2);
	}

	@Test
	void testParseNegativeMissingPuzzleElement() {
		/* MISSING_ELEMENT("Missing puzzle element(s) with the following IDs: <%s>") */
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add("1 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(1);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Missing puzzle element(s) with the following IDs: 2"));
	}

	@Test
	void testParseNegativeElementIdExeedsSize() {
		// ID_EXEEDS_SIZE("Puzzle of size <%s> cannot have the following IDs: <%s>")
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add("1 1 1 1 1");
		linesFromFile.add("2 1 1 1 1");
		linesFromFile.add("3 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(1);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Puzzle of size <2> cannot have the following IDs: 3"));
	}

	@Test
	void testParseNegativeWrongElementFormatEdgeInvalid() {
		// WRONG_ELEMENT_FORMAT("Puzzle ID <%s> has wrong data: <%s>")
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add("1 1 1 1 -5");
		linesFromFile.add("2 1 1 1 2");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(3);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Missing puzzle element(s) with the following IDs: 1, 2"));
		assertThat((errorsManager.getErrorsMap().get(1))).isEqualTo(("Puzzle ID <1> has wrong data: <1 1 1 1 -5>"));
		assertThat((errorsManager.getErrorsMap().get(2))).isEqualTo(("Puzzle ID <2> has wrong data: <2 1 1 1 2>"));
	}

	@Test
	void testParseNegativeWrongElementFormatTooManyEdges() {
		// WRONG_ELEMENT_FORMAT("Puzzle ID <%s> has wrong data: <%s>")
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add("1 1 1 1 1");
		linesFromFile.add("2 1 1 1 -1 0");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(2);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Missing puzzle element(s) with the following IDs: 2"));
		assertThat((errorsManager.getErrorsMap().get(1))).isEqualTo(("Puzzle ID <2> has wrong data: <2 1 1 1 -1 0>"));
	}

	@Test
	void testParseNegativeFirstLineErrorNumElementsBadFormat() {
		// FIRST_LINE_BEFORE_SIGN("First line must start with <NumElements>, instead was
		// <%s>")
		linesFromFile.add("NumberOfElements=2");
		linesFromFile.add("1 1 1 1 1");
		linesFromFile.add("2 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(1);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("First line must start with <NumElements>, instead was <NumberOfElements>"));
	}

	@Test
	void testParseNegativeFirstLineErrorValueBadFormat() {
		// FIRST_LINE_AFTER_SIGN("First line must end with an integer representing the
		// number of elements, instead was <%s>")
		linesFromFile.add("NumElements=TWO");
		linesFromFile.add("1 1 1 1 1");
		linesFromFile.add("2 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(2);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Puzzle of size <0> cannot have the following IDs: 1, 2"));
		assertThat((errorsManager.getErrorsMap().get(1))).isEqualTo(
		        ("First line must end with an integer representing the number of elements, instead was <TWO>"));
	}

	@Test
	void testParseNegativePieceEdgeNotNumeric() {
		// PIECE_VALUE_NOT_NUMERIC("All piece values must be numeric, instead got <%s>
		// on line <%s>")
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add("1 1 ONE 1 1");
		linesFromFile.add("2 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);

		parser.parse();
		assertThat((errorsManager.getErrorsMap())).hasSize(2);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Missing puzzle element(s) with the following IDs: 1"));
		assertThat((errorsManager.getErrorsMap().get(1)))
		        .isEqualTo(("All piece values must be numeric, instead got <ONE> on line <1>"));
	}

	@Test
	void testParseNegativeDuplicatedElementIds() {
		// DUPLICATE_IDS("Duplicate IDS found in the input file, <%s>")
		linesFromFile.add(NUM_ELEMENTS_2);
		linesFromFile.add("1 1 1 1 1");
		linesFromFile.add("1 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(2);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Missing puzzle element(s) with the following IDs: 2"));
		assertThat((errorsManager.getErrorsMap().get(1))).isEqualTo(("Duplicate IDS found in the input file, <[1]>"));
	}

	@Test
	void testParseNegativeMultipleErrorsInOrder() {
		linesFromFile.add("NumElements=5");
		linesFromFile.add("1 1 ONE 1 1");
		linesFromFile.add("2 1 1 1 1");
		linesFromFile.add("3 1 1 1 -5");
		linesFromFile.add("4 1 1 1 1 0");
		linesFromFile.add("2 1 1 1 1");
		linesFromFile.add("10 1 1 1 1");

		parser = new Parser(linesFromFile, errorsManager);
		parser.parse();

		assertThat((errorsManager.getErrorsMap())).hasSize(6);
		assertThat((errorsManager.getErrorsMap().get(0)))
		        .isEqualTo(("Missing puzzle element(s) with the following IDs: 1, 3, 4, 5"));
		assertThat((errorsManager.getErrorsMap().get(1)))
		        .isEqualTo(("Puzzle of size <5> cannot have the following IDs: 10"));
		assertThat((errorsManager.getErrorsMap().get(2))).isEqualTo(("Puzzle ID <3> has wrong data: <3 1 1 1 -5>"));
		assertThat((errorsManager.getErrorsMap().get(3))).isEqualTo(("Puzzle ID <4> has wrong data: <4 1 1 1 1 0>"));
		assertThat((errorsManager.getErrorsMap().get(4)))
		        .isEqualTo(("All piece values must be numeric, instead got <ONE> on line <1>"));
		assertThat((errorsManager.getErrorsMap().get(5))).isEqualTo(("Duplicate IDS found in the input file, <[2]>"));
	}

}
